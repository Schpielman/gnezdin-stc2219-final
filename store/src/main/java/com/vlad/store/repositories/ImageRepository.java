package com.vlad.store.repositories;

import com.vlad.store.models.Image ;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {
}
