package com.vlad.store.services;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class DateService {

    public Date date(){
        return new Date();
    }

    public String getCurrentMonth(){
        return LocalDateTime.now().getMonth().name();
    }
}

